from flask import Blueprint


"""
This module instance the Blueprint, and add el url prefix /auth
"""

auth = Blueprint('auth', __name__, url_prefix='/auth')


"""
Load the views.
"""
from . import views
