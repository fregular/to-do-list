
"""
All settings go here. The settings
are imported in app.__init__
"""

class Config:
    SECRET_KEY = 'SUPER SECRET'
    PRESERVE_CONTEXT_ON_EXCEPTION = False


