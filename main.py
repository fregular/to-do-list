from flask import  request, make_response, redirect, render_template, session,url_for, flash
import unittest
from flask_login import login_required, current_user
from app import create_app
from app.firestore_service import update_todo, get_todos, put_todo, delete_todo
app = create_app()
from app.forms import TodoForm


"""
This def load the test from the directory 'tests'.
"""
@app.cli.command()
def test():
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner().run(tests)


"""
The def not_found and server_error handler the error sending to 404.html or 500.html templates.
"""
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html', error=error)

@app.errorhandler(500)
def server_error(error):
    return render_template('500.html', error=error)


@app.route('/')
def index():
    """
    Get the ip, and save the ip in a cookie.
    Then return the response and response.
    The route '/' redirect to '/hello' that 
    receive the 'user_ip' and then print ip
    """
    user_ip = request.remote_addr
    response = make_response(redirect('/hello'))
    session['user_ip'] = user_ip 

    return response



"""
This def get the user_ip and username, then add this vars in the context.
Finally, render in helllo.html sending the context as a dict
"""

@app.route('/hello', methods=['GET','POST'])
@login_required
def hello():
    user_ip = session.get('user_ip')
    username = current_user.id
    todo_form = TodoForm()

    context = {
        'user_ip': user_ip,
        'todos': get_todos(user_id=username),
        'username': username,
        'todo_form': todo_form,
    }

    if todo_form.validate_on_submit():
        put_todo(user_id=username, description = todo_form.description.data)
        flash('Tu tarea se creo con exito!')
        return redirect(url_for('hello'))
    return render_template('hello.html', **context)

@app.route('/todos/delete/<todo_id>', methods=['GET','POST'])
def delete(todo_id):
    user_id = current_user.id
    delete_todo(user_id=user_id, todo_id=todo_id)
    return redirect(url_for('hello'))



@app.route('/todos/update/<todo_id>/<int:done>', methods=['GET','POST'])
def update(todo_id, done):
    user_id= current_user.id
    update_todo(user_id=user_id, todo_id=todo_id, done=done)
    return redirect(url_for('hello'))

